require 'minitest/autorun'
require 'webdrivers/chromedriver'
require_relative '../../lib/lunch_scraper'

class LunchScraperTest < MiniTest::Spec
  describe "Getting today's lunch" do
    it 'can parse out the correct lunch items' do
      @lunch = LunchScraper.new(Date.today)
      @lunch.today.size.must_equal 6
      @lunch.today.first.must_equal 'French Bread Pizza'
    end
  end
end
