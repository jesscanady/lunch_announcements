# Lunch Announcements

A very specific utility for scraping my eldest son's elementary school lunch menu site and texting us what he's having for lunch that day.

## Requirements

* Ruby
* Nokogiri / Watir
* [chrome webdriver](https://github.com/SeleniumHQ/selenium/wiki/ChromeDriver)

## ENV vars required:

* TO - the numbers to send to, comma delimited
* FROM - the Twilio number to send from
* TWILIO_ACCOUNT_SID - The Twilio Account SID
* TWILIO_AUTH_TOKEN - The Twilio secret auth token
