#!/usr/bin/env ruby
# coding: utf-8
#
# Send a Lunch Announcement.
#
# ENV vars required:
# - TO - the numbers to send to, comma delimited
# - FROM - the Twilio number to send from
# - TWILIO_ACCOUNT_SID - The Twilio Account SID
# - TWILIO_AUTH_TOKEN - The Twilio secret auth token
require 'date'
require 'twilio-ruby'
require_relative 'lib/lunch_scraper'

REQUIRED_ENV = %w(TO FROM TWILIO_ACCOUNT_SID TWILIO_AUTH_TOKEN).freeze

REQUIRED_ENV.each do |req|
  raise "Missing ENV var #{req}" if ENV[req].nil?
end

lunch = LunchScraper.new(Date.today)
menu = lunch.today

if menu.count.zero?
  STDERR.puts 'No menu items found.'
  exit 1
end

client = Twilio::REST::Client.new(ENV['TWILIO_ACCOUNT_SID'], ENV['TWILIO_AUTH_TOKEN'])

destinations = ENV['TO'].split(',')

destinations.each do |to|
  client.messages.create(
    from: ENV['FROM'],
    to: to,
    body: "TODAY'S FOUSE LUNCH MENU:\n\n#{menu.join("\n")}\n\n❤ Lunchbot️️️"
  )
end
