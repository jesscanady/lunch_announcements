require 'watir'
require 'nokogiri'

class LunchScraper
  def initialize(date)
    @date = date
    @url  = "https://westerville.nutrislice.com/menu/fouse-elementary/lunch/#{@date.strftime('%Y-%m-%d')}"
  end

  def today
    browser = Watir::Browser.new :chrome, headless: true
    browser.goto @url
    # browser.wait_until { |b| b.h3(class: ['day-label', 'today']).present? }
    browser.wait_until { |b| b.button(text: 'View Menus').present? }
    browser.button(text: 'View Menus').click

    browser.p(class: 'no-data').wait_while(&:exists?)
    browser.wait_until { |b| b.h3(class: ['day-label', 'today']).present? }

    page = Nokogiri::HTML(browser.html)
    page.css('h3.day-label.today ~ ul.items li a.food-name-inner').map(&:content)
  end
end
